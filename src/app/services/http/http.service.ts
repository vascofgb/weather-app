import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EventsService } from '../events/events.service';

@Injectable({
  providedIn: 'root'
})
/**
 * Service for http requests
 * 
 * Wraps the calls to the HttpClient in order to handle http errors accordingly
 */
export class HttpService {

  get<T>(url:string) : Observable<T> {
    return this.http.get<T>(url).pipe(catchError(this.handleError<T>()));
  }

  post<T>(url:string, body:any) : Observable<T> {
    return this.http.post<T>(url, body).pipe(catchError(this.handleError<T>()));
  }

  /**
   * Handles http errors and lets the app keep running by returning an empty result
   */
  private handleError<T>() {

    return (error: any): Observable<T> => {

      this.eventsService.emitAsync({name: "serverRequestFailed"});
      console.error(error);
      return of(null);
    };
  }

  constructor(private http: HttpClient, private eventsService:EventsService) { }
}
