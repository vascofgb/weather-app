/**
 * Interface to model city weather data
 */
export interface CityWeather {

    /**
     * The city average temperature in ºC
     */
    temperature:number;
    /**
     * The city average wind in miles per hour
     */
    wind:number;
    /**
     * A description of the weather condition
     */
    description:string;
    /**
     * An icon code of the weather condition
     */
    icon:string;
}

/**
 * Interface to model a city forecast entry
 */
export interface CityForecastEntry extends CityWeather {

    /**
     * The timestamp of the forecast entry in milliseconds
     */
    time:number;
}