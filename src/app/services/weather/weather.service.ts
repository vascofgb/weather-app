import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { ConfigService } from '../config/config.service';
import { CityWeather, CityForecastEntry } from './weather';

import { environment } from 'src/environments/environment'; 

@Injectable({
  providedIn: 'root'
})
/**
 * Weather service which is the service used to call the Open Weather Data API services
 */
export class WeatherService {

  private config:any;

  /**
   * Requests the weather endpoint of the Open Weather Data API to retrieve
   * weather related data for the given city
   * 
   * @param cityName The city name (one of Lisbon, London, Dublin, Amsterdam or Rio de Janeiro)
   * @param countryCode The country code (one of pt, en, ir, nl or br)
   */
  getWeather(cityName:string, countryCode:string) {

    let url = this.config.weatherApi.weatherUrl;
    url = url.replace(/apiKey/i, environment.openWeatherDataAPIKey);
    url = url.replace(/city/i, cityName);
    url = url.replace(/country/i, countryCode);
    return this.httpService.get<any>(url);
  }

  /**
   * Requests the forecast endpoint of the Open Weather Data API to retrieve
   * forecast hourly data for the given city
   *  
   * @param cityName The city name (one of Lisbon, London, Dublin, Amsterdam or Rio de Janeiro)
   * @param countryCode The country code (one of pt, en, ir, nl or br)
   */
  getForecast(cityName:string, countryCode:string) {

    let url = this.config.weatherApi.forecastUrl;
    url = url.replace(/apiKey/i, environment.openWeatherDataAPIKey);
    url = url.replace(/city/i, cityName);
    url = url.replace(/country/i, countryCode);
    return this.httpService.get<any>(url);
  }

  /**
   * Parses the response of a call to the weather endpoint of the Open Weather Data API 
   * returning an object implementing the CityWeather interface
   * 
   * @param response A city weather object implementing the CityWeather interface
   */
  parseWeatherResult(response:any):CityWeather {
    if(response && response.weather && response.weather.length > 0) {
      let weather = response.weather[0];
      return {
        temperature: response.main ? response.main.temp : undefined,
        wind: response.wind ? response.wind.speed : undefined,
        description: weather.description,
        icon: weather.icon
      };
    } else {
      return null;
    }
  }

  /**
   * Parses the response of a call to the forecast endpoint of the Open Weather Data API 
   * returning an array of objects implementing the CityForecastEntry interface
   *  
   * @param response An array of forecast entries implementing the CityForecastEntry interface
   */
  parseForecastResult(response:any):CityForecastEntry[] {

    let forecast:CityForecastEntry[] = [];

    if(response && response.list) {
      for(let entry of response.list) {
        let weather = entry.weather && entry.weather.length > 0 ? entry.weather[0] : undefined;
        forecast.push({
          time: entry.dt,
          temperature: entry.main ? entry.main.temp : undefined,
          wind: entry.wind ? entry.wind.speed : undefined,
          description: weather.description,
          icon: weather.icon
        });
      }
    }

    return forecast;
  }

  constructor(private httpService:HttpService, private configService:ConfigService) { 
    this.config = this.configService.getConfiguration();
  }
}
