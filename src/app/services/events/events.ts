/**
 * Interface to model an application event
 */
export interface AppEvent {

    /**
     * The name of the event which can be any non empty string
     */
    name:string;

    /**
     * The name of the context associated with this event. This is used to filter
     * the target listeners to only the ones associated with this context
     */
    context?:string;

    /**
     * A data object which can be anything depending on the event itself
     */
    data?:any;
}