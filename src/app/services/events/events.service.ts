import { Injectable, EventEmitter } from '@angular/core';
import { AppEvent } from './events';

@Injectable({
  providedIn: 'root'
})
/**
 * Events service used to centralize the raise and subscription of application events
 */
export class EventsService {

  emitter = new EventEmitter<AppEvent>(false);
  asyncEmitter = new EventEmitter<AppEvent>(true);

  /**
   * Emits the given event synchronously
   * 
   * @param event An event object implementing the AppEvent interface 
   */
  emit(event:AppEvent) {
    this.emitter.emit(event);
  }

  /**
   * Emits the given event asynchronously
   * 
   * @param event An event object implementing the AppEvent interface 
   */
  emitAsync(event:AppEvent) {
    this.asyncEmitter.emit(event);
  }

  /**
   * Subscribes to application synchronous events
   * 
   * @param generatorOrNext 
   */
  subscribe(generatorOrNext?:any) {
    return this.emitter.subscribe(generatorOrNext);
  }

  /**
   * Subscribes to application asynchronous events
   * 
   * @param generatorOrNext 
   */
  subscribeAsync(generatorOrNext?:any) {
    return this.asyncEmitter.subscribe(generatorOrNext);
  }

  constructor() { }
}
