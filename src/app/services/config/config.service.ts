import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
/**
 * Service for application related configuration
 */
export class ConfigService {

  private configuration:any = {
    cities: [
      {
        key: "lisbon",
        name: "Lisbon",
        country: "pt"
      }, 
      {
        key: "london",
        name: "London",
        country: "uk"
      }, 
      {
        key: "dublin",
        name: "Dublin",
        country: "ie"
      }, 
      {
        key: "amsterdam",
        name: "Amsterdam",
        country: "nl"
      }, 
      {
        key: "rio",
        name: "Rio de Janeiro",
        country: "br"
      }
    ],
    weatherApi : {
      weatherUrl: "http://api.openweathermap.org/data/2.5/weather?q=city,country&units=metric&appid=apiKey",
      forecastUrl: "http://api.openweathermap.org/data/2.5/forecast?q=city,country&units=metric&appid=apiKey"
    }
  }

  /**
   * Gets the loaded configuration properties
   */
  getConfiguration() {
    return Object.assign({}, this.configuration);
  }

  constructor() { }
}

