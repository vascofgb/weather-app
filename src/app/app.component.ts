import { Component, ViewChild } from '@angular/core';
import { EventsService } from './services/events/events.service';
import { Subscription } from 'rxjs';
import { AppEvent } from './services/events/events';

declare var M;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private subscription:Subscription;
  private asyncSubscription:Subscription;
  private toast:boolean = false;

  /**
   * Emits the "refresh" event that should be handled
   * by the application components
   */
  refresh() {
    this.eventsService.emit({name: "refresh"});
  }

  /**
   * Emits the "openSideNav" event that should be handled
   * by the weather-sidenav component
   */
  openSideNav() {
    this.eventsService.emit({name: "openSideNav"});
  }

  /**
   * Handles an event raised by the events service
   * 
   * @param event An event {name:string, data:any} 
   */
  private handleEvent(event:AppEvent) {
    if(event.name == "serverRequestFailed") {
      if(!this.toast) {
        this.toast = true;
        let comp = this;
        M.toast({html: 'Sorry but there was a problem contacting the server.  If you continue to see this<br/>message check your internet connection or contact technical support team', completeCallback: function() {
          comp.toast = false;
        }});
      }
    }
  }

  /**
   * Subscribes application events
   */
  private initializeEvents() {

    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(this.asyncSubscription) {
      this.asyncSubscription.unsubscribe();
    }
    
    this.subscription = this.eventsService.subscribe((event:AppEvent)=> {
      try {
        this.handleEvent(event);
      } catch(e) {
        console.log(e);
      }
    });

    this.asyncSubscription =  this.eventsService.subscribeAsync((event:AppEvent)=> {
      try {
        this.handleEvent(event);
      } catch(e) {
        console.log(e);
      }
    });
  }

  constructor(private eventsService:EventsService) {
    this.initializeEvents();
  }

  ngOnDestroy() {

    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    if(!this.asyncSubscription) {
      this.asyncSubscription.unsubscribe();
    }
  }
}
