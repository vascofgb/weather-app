import { Component, OnInit, Input } from '@angular/core';
import { EventsService } from 'src/app/services/events/events.service';
import { CityForecastEntry } from 'src/app/services/weather/weather';
import { AppEvent } from 'src/app/services/events/events';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-weather-barchart',
  templateUrl: './weather-barchart.component.html',
  styleUrls: ['./weather-barchart.component.css']
})
export class WeatherBarchartComponent implements OnInit {

  private _entries:CityForecastEntry[];
  get entries():CityForecastEntry[] {
      return this._entries;
  }
  
  @Input('entries')
  set entries(value:CityForecastEntry[]) {
      this._entries = value;
      this.refresh();
  }

  labels:string[];
  type:string = "bar";
  data:any[] = [];
  legend:boolean = true;
  options:any = {
    scaleShowVerticalLines: false,
    maintainAspectRatio: false,
    responsive: true,
    legend: {
      display: true,
      position: 'bottom'
    },
    scales: {
      xAxes: [{
        display: false
      }],
      yAxes: [{
        display: false
      }]
    }
  };

  private refresh() {

    this.data = [];
    this.labels = [];
    let temperatures = [];
    let winds = [];
    this.data.push({label: "Temperature", data: temperatures, backgroundColor: "#ffdd37", borderColor: "#ffdd37", borderWidth: 1});
    this.data.push({label: "Wind", data: winds, backgroundColor: "#a3d4f7", borderColor: "#a3d4f7", borderWidth: 1});
    for(let entry of this.entries) {
      temperatures.push(entry.temperature);
      winds.push(entry.wind);
      this.labels.push("");
    }
  }

  constructor(private eventsService:EventsService) { 
  }

  ngOnInit() {
  }
}
