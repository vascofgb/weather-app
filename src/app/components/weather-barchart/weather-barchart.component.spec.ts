import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherBarchartComponent } from './weather-barchart.component';

describe('WeatherBarchartComponent', () => {
  let component: WeatherBarchartComponent;
  let fixture: ComponentFixture<WeatherBarchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherBarchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherBarchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
