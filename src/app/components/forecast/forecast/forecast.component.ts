import { Component, OnInit, Input } from '@angular/core';
import { CityForecastEntry } from 'src/app/services/weather/weather';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
/**
 * Component that models the forecast table and displays the forecast hourly information
 * for a given city
 */
export class ForecastComponent implements OnInit {

  private _entries:CityForecastEntry[];
  get entries():CityForecastEntry[] {
      return this._entries;
  }
  
  @Input('entries')
  set entries(value:CityForecastEntry[]) {
      this._entries = value;
      this.refresh();
  }

  dayHeaders:number[];
  dayColSpans:number[];
  timeHeaders:number[];
  temperatures:string[];
  winds:string[];
  icons:string[];

  /**
   * Refreshes the forecast table
   */
  private refresh() {
    let currentColSpan = 1;
    let index = 0;
    this.dayHeaders = [];
    this.dayColSpans = [];
    this.timeHeaders = [];
    this.temperatures = [];
    this.winds = [];
    this.icons = [];
    if(this.entries) {
      for(let entry of this.entries) {

        let time = entry.time*1000;
        if(index == 0) {
          this.dayHeaders.push(time);
          currentColSpan = 1;
          index++;
        } else if(!this.isSameDay(this.dayHeaders[index-1], time)) {
          this.dayHeaders.push(time);
          this.dayColSpans.push(currentColSpan);
          currentColSpan = 1;
          index++;
        } else {
          currentColSpan++;
        }

        this.timeHeaders.push(time);
        this.temperatures.push(Math.round(entry.temperature).toString());
        this.winds.push(entry.wind.toString());
        this.icons.push(entry.icon);
        
      }

      this.dayColSpans.push(currentColSpan);
    }
  }

  /**
   * Utility method to check if two dates point to the same day
   * 
   * @param time1 A date in milliseconds 
   * @param time2 A date in milliseconds
   */
  private isSameDay(time1:number, time2:number) {
    let date1 = new Date(time1);
    let date2 = new Date(time2);
    return date1.getFullYear() === date2.getFullYear() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getDate() === date2.getDate();
  }

  constructor() { 
    this.refresh();
  }

  ngOnInit() {
  }

}
