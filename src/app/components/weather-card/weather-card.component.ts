import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.css']
})
/**
 * Component that models a city weather card
 */
export class WeatherCardComponent implements OnInit {

  @Input() temperature:number;
  @Input() wind:number;
  @Input() name:string;
  @Input() description:string;
  @Input() icon:string;
  @Input() active:boolean;
  @Input() loading:boolean;
  @Input() error:boolean;

  constructor() { }

  ngOnInit() {
  }

}
