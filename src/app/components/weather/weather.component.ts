import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config/config.service';
import { EventsService } from 'src/app/services/events/events.service';
import { WeatherService } from 'src/app/services/weather/weather.service';
import { AppEvent } from 'src/app/services/events/events';
import { CityWeather, CityForecastEntry } from 'src/app/services/weather/weather';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
/**
 * Component that models the main weather view that contains the weather cards, the forecast table
 * and the forecast chart
 */
export class WeatherComponent implements OnInit {

  config:any;
  cities:any[] = [];
  forecast:CityForecastEntry[] = [];
  loadingForecast:boolean = true;
  forecastError:boolean = false;
  selectedCity:any = null;
  private subscription:Subscription = null;

  /**
   * Selects the card for the given city
   * 
   * @param city A city object containing the main information to display in the component 
   */
  selectCityCard(city:any) {

    this.selectedCity = city;
    for(let city of this.cities) {
      city.selected = false;
    }
    city.selected = true;
    this.loadCityForecast(city);
    this.eventsService.emit({name: "citySelected", data: {city: city.name, weather: city.weather}})
  }

  /**
   * Loads the city weather info by calling the weather endpoints of the Open Weather Data API 
   * for all the configured cities
   */
  private loadCityWeathers() {

    let index = 0;
    for(let city of this.cities) {
      city.loading = true;
      city.error = false;
      this.weatherService.getWeather(city.name, city.country).subscribe((response:any) => {

        let result:CityWeather = this.weatherService.parseWeatherResult(response);
        city.loading = false;
        city.error = result == null;
        city.weather = result;
        if(result) {
          city.description = result.description;
          city.icon = result.icon;
          city.wind = result.wind;
          city.temperature = result.temperature ? Math.round(result.temperature) : undefined;
        } else {
          city.description = undefined;
          city.icon = undefined;
          city.wind = undefined;
          city.temperature = undefined;
        }

        city.selected = city.name === this.selectedCity.name;
        if(city.selected) {
          this.selectCityCard(city);
        }
        index++;
      });
    }

    if(this.selectedCity == null) {
      this.selectCityCard(this.cities[0]);
    }
  }

  /**
   * Loads the forecast by calling the forecast endpoint of the Open Weather Data API
   * for the given city
   */
  private loadCityForecast(city:any) {
    this.loadingForecast = true;
    this.weatherService.getForecast(city.name, city.country).subscribe((response:any) => {
      this.loadingForecast = false;
      this.forecast = this.weatherService.parseForecastResult(response);
      if(this.forecast && this.forecast.length > 0) {
        this.forecastError = false;
      } else {
        this.forecastError = true;
      }

      this.eventsService.emit({name: "forecast", data: {forecast: this.forecast}});
    });
  }

  /**
   * Gets the city object by its name
   * 
   * @param name The city name (one of Lisbon, London, Dublin, Amsterdam or Rio de Janeiro) 
   */
  private getCity(name:string) {
    for(let city of this.cities) {
      if(city.name == name) {
        return city;
      }
    }

    return null;
  }

  /**
   * Initializes the city cards
   */
  private initializeCityCards() {

    this.cities = [];
    for(let city of this.config.cities) {
      this.cities.push({
        key: city.key,
        name: city.name,
        country: city.country,
        loading: true,
        error: false,
        selected: false,
        description: null,
        temperature: null,
        wind: null,
        icon: ""
      });
    }

    if(this.cities.length > 0) {
      this.cities[0].selected = true;
    }
  }

  /**
   * Subscribes application events
   */
  private initializeEvents() {

    if(this.subscription) {
      this.subscription.unsubscribe();
    }

    this.subscription = this.eventsService.subscribe((event:AppEvent)=> {
      try {
        this.handleEvent(event);
      } catch(e) {
        console.log(e);
      }
    });
  }

  /**
   * Handles the given application event
   * 
   * @param event An event object implementing the AppEvent interface
   */
  private handleEvent(event:AppEvent) {
    if(event.name == "refresh") {
      this.loadCityWeathers();
    } else if(event.name == "select") {
      let city = this.getCity(event.data.name);
      this.selectCityCard(city);
    }
  }

  constructor(private eventsService:EventsService, private configService:ConfigService, private weatherService:WeatherService) { 
    this.config = this.configService.getConfiguration();
    this.initializeEvents();
    this.initializeCityCards();
    this.loadCityWeathers();
  }

  ngOnInit() {
  }

  ngOnDestroy() {

    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
