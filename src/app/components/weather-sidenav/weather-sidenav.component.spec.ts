import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherSidenavComponent } from './weather-sidenav.component';

describe('WeatherSidenavComponent', () => {
  let component: WeatherSidenavComponent;
  let fixture: ComponentFixture<WeatherSidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherSidenavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherSidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
