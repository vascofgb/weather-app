import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfigService } from 'src/app/services/config/config.service';
import { EventsService } from 'src/app/services/events/events.service';
import { CityWeather } from 'src/app/services/weather/weather';
import { AppEvent } from 'src/app/services/events/events';
import { Subscription } from 'rxjs';

declare var M;

@Component({
  selector: 'app-weather-sidenav',
  templateUrl: './weather-sidenav.component.html',
  styleUrls: ['./weather-sidenav.component.css']
})
/**
 * Component that models the side navigation panel shown in small devices
 */
export class WeatherSidenavComponent implements OnInit {

  @ViewChild("sideNav") sideNav;
  config:any;
  cities:string[] = [];
  currentCity:any = null;
  instance:any = null;
  private subscription:Subscription = null;

  /**
   * Selects the given city by the emitting the "select" event which should
   * be handled by the WeatherComponent
   * 
   * @param name The city name (one of Lisbon, London, Dublin, Amsterdam or Rio de Janeiro) 
   */
  selectCity(name:string) {
    this.close();
    this.eventsService.emit({name: "select", data: {name: name}});
  }

  /**
   * Opens this side navigation panel
   */
  open() {
    if(this.instance) {
      this.instance.open();
    }  
  }

  /**
   * Closes this side navigation panel
   */
  close() {
    if(this.instance) {
      this.instance.close();
    } 
  }

  /**
   * Initializes the side navigation panel as 
   */
  private initializeSideNav() {
    this.instance = M.Sidenav.init(this.sideNav.nativeElement, {draggable: true});
  }

  /**
   * Initializes city names
   */
  private initializeCities() {
    for(let city of this.config.cities) {
      this.cities.push(city.name);
    }
  }

  /**
   * Subscribes application events
   */
  private initializeEvents() {

    if(this.subscription) {
      this.subscription.unsubscribe();
    }
    
    this.subscription = this.eventsService.subscribe((event:AppEvent)=> {
      try {
        this.handleEvent(event);
      } catch(e) {
        console.log(e);
      }
    });
  }

  /**
   * Handles the event of a new selected street in order to update the
   * current city information shown by this side navigation panel
   * 
   * @param city The city name (one of Lisbon, London, Dublin, Amsterdam or Rio de Janeiro)
   * @param weather The city weather data object implementing the CityWeather interface
   */
  private handleNewCurrentCity(city:string, weather:CityWeather) {
    this.currentCity = {
      name: city,
      temperature: weather ? Math.round(weather.temperature) : undefined,
      description: weather ? weather.description : undefined,
      icon: weather ? weather.icon : undefined
    }
  }

  /**
   * Handles the given application event
   * 
   * @param event An event object implementing the AppEvent interface
   */
  private handleEvent(event:AppEvent) {

    if(event.name == "citySelected") {
      let city:string = event.data.city as string;
      let weather:CityWeather = event.data.weather as CityWeather;
      this.handleNewCurrentCity(city, weather);
    } else if(event.name == "openSideNav") {
      this.open();
    }
  }

  constructor(private eventsService:EventsService, private configService:ConfigService) { 
    this.config = this.configService.getConfiguration();
    this.initializeCities();
    this.initializeEvents();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initializeSideNav();
  }

  ngOnDestroy() {

    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
