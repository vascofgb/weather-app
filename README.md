# WeatherApp

This application was developed following the Backbase Frontend Developer Exercise 2.2
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.7.

## Run application

The application can be run simply by opening the index.html file in dist/weather-app folder. In alternative it is possible run `ng serve` for a dev server and navigate to `http://localhost:4200/`

## Build application

If a build is necessary run `ng build --prod` to update the dist folder (a `npm install` must be ensured previously)

## Open Weather Data API

The application uses the Open Weather Data API to obtain weather and forecast info. To use this API a valid authorization key is needed. Currently the application is using the key: 142ddfb40a94b3501ab84ade2c4fa7dc so before running the application check the validaty of the api key using the following links:

http://api.openweathermap.org/data/2.5/forecast?q=London,uk&units=metric&appid=3d8b309701a13f65b660fa2c64cdc517 
http://api.openweathermap.org/data/2.5/weather?q=London,uk&units=metric&appid=3d8b309701a13f65b660fa2c64cdc517

To change the api key update the environment.ts and environment.prod.ts files. If so a new build to the dist folder will be required so execute ng build --prod

## Main features

The application displays a set of five cards each one containing weather information about a city, meaning average temperature, average wind speed and a weather condition.

By clicking/selecting one card a hourly forecast table and chart for the selected city will be shown. The user can slide horizontally to view the data for the next days

## Components

The application is supported by five custom components:

* The AppComponent implements the outter layout, including header, main and footer sections

* The WeatherCardComponent implements a city weather card

* The ForecastComponent implements the forecast table

* The WeatherBarchartComponent implements the forecast chart

* The WeatherSidenavComponent implements the side navigation panel used for small devices

## Services

The application is supported by four custom services:

* The EventsService which is used by the application components to emit and subscribe to application related events. This is how the AppComponent communicates with the WeatherComponent for instance.

* The HttpService which is a wrap to the HttpClient to handle and notify http errors

* The ConfigService which holds application related configuration

* The WeatherService which is the service used to call the Open Weather Data API endpoints

## Small devices

The application adapts to small devices by showing one card at a time. For small devices the card selection can be done by using the side navigation panel which can be opened using the header menu icon or by using swipe gestures.

## Browsers

The application was tested with Google Chrome 74.0.3729.157 and Firefox 51.0.1
